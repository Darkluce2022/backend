//Agrego Middleware
const verificarGet = require("../middleware/verificarGet")

//creamos el módulo a exportar
//Al ser llamado en index.js recibe las capacidades de express, para ser utilizado
module.exports = function (app) {

        // Creamos la ruta obtener todos los productos.
        app.get("/manzana", async function (req, res) {
            //requerimos y guardamos la ruta de services donde hara la consulta a la base
            const clientes = require("./../services/clientesServices")
            //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
            //podemos seguir viendo el proceso en  clientesServices.
            const response = await clientes.getClientes()
            //Una vez recibida la respuesta, se la mandamos a la ruta
            res.send(response.result)
        })

        
function consoleComienzo (req, res, next){
    console.log("hiciste una consulta")
        next();
   
}


        app.get("/cliente/:id", consoleComienzo, verificarGet.verificarNumero, async function (req, res) {
            
            // tomamos el parametro de la ruta
            const id = req.params.id
            const clientes = require("./../services/clientesServices")
            const response = await clientes.getClienteById(id)
            if (response.error) {
                res.send(response.error)
            } else{
                res.send(response.result) 
            }
        })

        // enviar un producto por body y retornarlo
        app.post("/cliente", async function(req, res) {
            //tomamos datos del body
            const productoNuevo = req.body
            const clientes = require("./../services/clientesServices")
            const response = await clientes.postCliente(productoNuevo)
            if (response.error) {
                res.send(response.error)
            } else{
                res.send(response.result) 
            }
        })
}