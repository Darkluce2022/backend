module.exports = function (app) {
    
    app.get("/rol", async function (req, res) {
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const rol = require("./../services/rolService")
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await rol.getRol()
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result)
    })
    app.get("/rol/:id", async function (req, res) {
        // tomamos el parametro de la ruta
        const id = req.params.id
        const rol = require("./../services/rolService")
        const response = await rol.getRolById(id)
        if (response.error) {
            res.send(response.error)
        } else {
            res.send(response.result)
        }
    })
}
