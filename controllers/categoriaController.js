module.exports = function (app) {
    
    app.get("/categoria", async function (req, res) {
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const categoria = require("./../services/categoriaService")
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await categoria.getCategoria()
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result)
    })
    app.get("/id", async function (req, res) {
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const id = require("./../services/categoriaService")
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await id.getId()
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result)
    })
}
