module.exports = function (app) {
    
    app.get("/producto", async function (req, res) {
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const producto = require("./../services/productoService")
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await producto.getProducto()
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result)
    })
    
    app.get("/producto/:id", async function (req, res) {
            
        // tomamos el parametro de la ruta
        const id = req.params.id
        const clientes = require("./../services/productoService")
        const response = await clientes.getProductoById(id)
        if (response.error) {
            res.send(response.error)
        } else{
            res.send(response.result) 
        }
    })
}
