module.exports = function (app) {

    // Creamos la ruta obtener todos los productos.
    app.get("/accesorios", async function (req, res) {
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const accesorios = require("./../services/accesoriosServices")
        //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
        //podemos seguir viendo el proceso en  clientesServices.
        const response = await accesorios.getAccesorios()
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result)
    })
    app.get("/accesorios/:id", async function (req, res) {
            
        // tomamos el parametro de la ruta
        const id = req.params.id
        const accesorios = require("../services/accesoriosServices")
        const response = await accesorios.getAccesoriosById(id)
        if (response.error) {
            res.send(response.error)
        } else{
            res.send(response.result) 
        }
    })
    app.post("/accesorios", async function (req, res) {
            
        // tomamos el parametro de la ruta
        const id = req.body
        const accesorios = require("../services/accesoriosServices")
        const response = await accesorios.postAccesorios(id)
        if (response.error) {
            res.send(response.error)
        } else{
            res.send(response.result) 
        }
    })
    app.put("/accesorios", async function (req, res) {
            
        // tomamos el parametro de la ruta
        const id = req.body
        const accesorios = require("../services/accesoriosServices")
        const response = await accesorios.putAccesorios(id)
        if (response.error) {
            res.send(response.error)
        } else{
            res.send(response.result) 
        }
    })
}

