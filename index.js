// Requerimos express y aguardamos en una variable, que es un entorno de trabajo de node para poder trabajar con apis.
const express = require('express')
const app = express()


// body parser nos ayuda a deserializar el body de una request
const bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

process.env.SECRET = process.env.SECRET || 'lejtnoqhfrnzdoupbdwintdlhzhelisb';

const secret = process.env.SECRET;

//requerimos a controllers, donde vamos a tener las rutas de las consultas
//Luego dirígete a este punto para seguir viendo el proceso.
require("./controllers/loginController")(app)

require("./controllers/clientesController")(app)

require("./controllers/rolController")(app)

require("./controllers/usuariosController")(app)

require("./controllers/ventasController")(app)

require("./controllers/categoriaController")(app)
require("./controllers/productoController")(app)
require("./controllers/accesoriosController")(app)



app.get("/accesorios", async function (req, res) {

    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result)
})

//generamos el servidor

app.listen(3000, function () {
    console.log("servidor iniciado en el puerto 3000")
})