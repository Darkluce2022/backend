//requerimos el módulo para conectarse a la base de datos
const mysql = require('mysql')
//requerimos el archivo donde tenemos configurada la conexion
const conn = require('../config/conn')
//creamos la constante a ser exportada
const usuarios = {
    async getId() {

        //Guardamos en una variable la consulta que queremos generar
        let sql = 'SELECT * FROM categorias'
        //Con el archivo de conexion a la base, enviamos la consulta a la misma
        //Ponemos un await porque desconocemos la demora de la misma
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    }

    }
        //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
        
           
//Exportamos el módulo
module.exports = usuarios