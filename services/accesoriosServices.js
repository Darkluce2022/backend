//requerimos el módulo para conectarse a la base de datos
const mysql = require('mysql')
//requerimos el archivo donde tenemos configurada la conexion
const conn = require('../config/conn')
//creamos la constante a ser exportada
const accesorios = {
    async getAccesorios() {

        //Guardamos en una variable la consulta que queremos generar
        let sql = 'SELECT * FROM accesorios'
        //Con el archivo de conexion a la base, enviamos la consulta a la misma
        //Ponemos un await porque desconocemos la demora de la misma
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async getAccesoriosById (id) {
    
        let sql = 'SELECT * FROM accesorios WHERE id = ' + id
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },
    async postAccesorios(body) {

        //Guardamos en una variable la consulta que queremos generar
        let sql = "INSERT INTO `accesorios`(`id`, `tipo`) VALUES (NULL,'" + body.tipo +"')"
        //Con el archivo de conexion a la base, enviamos la consulta a la misma
        //Ponemos un await porque desconocemos la demora de la misma
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },
    async putAccesorios(body) {

        //Guardamos en una variable la consulta que queremos generar
        let sql = "UPDATE `accesorios` SET `tipo`='" + body.tipo + "' WHERE id =" + body.id 
        console.log(sql)
        //Con el archivo de conexion a la base, enviamos la consulta a la misma
        //Ponemos un await porque desconocemos la demora de la misma
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    }
        //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
        
           
//Exportamos el módulo
module.exports = accesorios